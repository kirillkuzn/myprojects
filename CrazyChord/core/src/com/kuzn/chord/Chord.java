package com.kuzn.chord;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.kuzn.chord.model.*;
import com.kuzn.chord.model.Point;
import com.kuzn.chord.service.Control;
import com.kuzn.chord.service.Drawer;
import com.kuzn.chord.service.PointPlacer;

import java.awt.*;

public class Chord extends ApplicationAdapter {
	ShapeRenderer shape;
	Circle circle;
	PointPlacer pp;
	Control control;
	Drawer drawer;
	LineList lineList;
	private int numberOfPoint = 10;
	
	@Override
	public void create () {
		shape = new ShapeRenderer();
		circle = new Circle(new Point(Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() / 2 ) , 200);
		pp = new PointPlacer(circle, numberOfPoint,shape);
		drawer = new Drawer(shape);
		lineList = new LineList();
		control = new Control(pp.getPointList(),drawer, lineList);
		Gdx.input.setInputProcessor(control);

		for (PointWithAction p: pp.getPointList()){
			p.setDefaultColor();
		}
	}

	@Override
	public void render () {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		shape.begin(ShapeRenderer.ShapeType.Line);
		shape.setColor(0,1,0,0);
		shape.circle(circle.getCenter().getPointX(), circle.getCenter().getPointY(), circle.getRadius());

		//Print point in circle
		for (PointWithAction p: pp.getPointList()){
			drawer.drawCircle(p, p.getColor());
		}

		//drawer.drawLine(new Line(new Point(122,212), new Point(494,338)), Color.WHITE);

		//Print line if they have
		for (Line l: lineList.getList()) {
			drawer.drawLine(l, l.getColor());

		}
		shape.end();
	}
	
	@Override
	public void dispose () {

		shape.dispose();
	}
}
