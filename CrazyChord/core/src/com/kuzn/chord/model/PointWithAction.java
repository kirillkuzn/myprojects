package com.kuzn.chord.model;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

public class PointWithAction extends Point{

    private int circleRadius = 10;
    private Color color;

    public PointWithAction(){super();}

    public PointWithAction(int x, int y){
        super(x,y);
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public void setDefaultColor(){
        color = Color.SKY;
    }

    public int getCircleRadius() {
        return circleRadius;
    }
}
