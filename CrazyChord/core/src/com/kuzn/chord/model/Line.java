package com.kuzn.chord.model;

import com.badlogic.gdx.graphics.Color;

public class Line {

    private Point point1;
    private Point point2;
    private Color color;
    private boolean canRemove;

    public Line(){}

    public Line(Point point1, Point point2){
        this.point1 = point1;
        this.point2 = point2;
        canRemove = true;
    }

    public Point getPoint1() {
        return point1;
    }

    public void setPoint1(Point point1) {
        this.point1 = point1;
    }

    public Point getPoint2() {
        return point2;
    }

    public void setPoint2(Point point2) {
        this.point2 = point2;
    }

    public void setDefaultColor(){
        color = Color.WHITE;
    }

    public void setColor(Color color){
        this.color = color;
    }

    public Color getColor(){
        return color;
    }

    public boolean isCanRemove() {
        return canRemove;
    }

    public void setCanRemove(boolean canRemove) {
        this.canRemove = canRemove;
    }

    public boolean checkForCollision(Line line2) {
        Point point3 = line2.getPoint1();
        Point point4 = line2.getPoint2();

        int det1 = (point3.getPointX() - point1.getPointX()) * (point2.getPointY() - point1.getPointY()) -
                (point3.getPointY() - point1.getPointY()) - (point2.getPointX() - point1.getPointX());
        int det2 = (point4.getPointX() - point1.getPointX()) * (point2.getPointY() - point1.getPointY()) -
                (point4.getPointY() - point1.getPointY()) - (point2.getPointX() - point1.getPointX());

        if ((det1 < 0 && det2 > 0) || (det1 > 0 && det2 < 0)){
            return true;
        }

        return false;
    }



    @Override
    public String toString() {
        return "Line{" +
                "point1=" + point1 +
                ", point2=" + point2 +
                '}';
    }
}
