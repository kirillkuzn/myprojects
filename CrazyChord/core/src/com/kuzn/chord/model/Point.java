package com.kuzn.chord.model;

import com.badlogic.gdx.InputAdapter;

public class Point  {

    private int pointX;
    private int pointY;

    public Point(){}

    public Point(int x, int y){
        this.pointX = x;
        this.pointY = y;
    }

    public int getPointX() {
        return pointX;
    }

    public void setPointX(int pointX) {
        this.pointX = pointX;
    }

    public int getPointY() {
        return pointY;
    }

    public void setPointY(int pointY) {
        this.pointY = pointY;
    }

    @Override
    public String toString() {
        return "Point{" +
                "pointX=" + pointX +
                ", pointY=" + pointY +
                '}';
    }


}
