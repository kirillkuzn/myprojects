package com.kuzn.chord.model;

import java.util.ArrayList;
import java.util.List;

public class LineList {

    private List<Line> list;

    public LineList(){
        list = new ArrayList<Line>();
    }

    public void addToList(Line line){
        list.add(line);
    }

    public void remove(Line line){
        list.remove(line);
    }

    public List<Line> getList() {
        return list;
    }


    public  int area (Point a, Point b, Point c) {
        return (b.getPointX() - a.getPointX()) * (c.getPointY() - a.getPointY()) - (b.getPointY() - a.getPointY()) * (c.getPointX() - a.getPointX());
    }

    public boolean intersect_1 (int a, int b, int c, int d) {
        int temp;
        if (a > b){
            temp = a;
            a = b;
            b = temp;
        }
        if (c > d){
            temp = c;
            c = d;
            d = temp;
        }
        return Math.max(a,c) <= Math.min(b,d);
    }

    public boolean intersect (Point a, Point b, Point c, Point d) {
        return intersect_1(a.getPointX(), b.getPointX(), c.getPointX(), d.getPointX())
                && intersect_1(a.getPointY(), b.getPointY(), c.getPointY(), d.getPointY())
                && area(a, b, c) * area(a, b, d) <= 0
                && area(c, d, a) * area(c, d, b) <= 0;
    }

    public boolean isCollision(Point a, Point b, Point c, Point d){
        int d1 = (c.getPointX() - a.getPointX()) * (b.getPointY() - a.getPointY()) -
                (c.getPointY() - a.getPointY()) * (b.getPointX() - a.getPointX());
        int d2 = (d.getPointX() - a.getPointX()) * (b.getPointY() - a.getPointY()) -
                (d.getPointY() - a.getPointY()) * (b.getPointX() - a.getPointX());
        if ( (d1 < 0 && d2 > 0) || (d1 > 0 && d2 < 0) ){
            return true;
        }
        return false;
    }


}
