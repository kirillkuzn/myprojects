package com.kuzn.chord.service;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Color;
import com.kuzn.chord.model.Line;
import com.kuzn.chord.model.LineList;
import com.kuzn.chord.model.Point;
import com.kuzn.chord.model.PointWithAction;

import java.util.List;

public class Control implements InputProcessor {

    private List<PointWithAction> pointList;
    private Drawer drawer;
    private Line line;
    private LineList lineList;

    public Control(List<PointWithAction> pointListist, Drawer drawer, LineList lineList){
        this.pointList = pointListist;
        this.drawer = drawer;
        this.lineList = lineList;
    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        screenY = Gdx.graphics.getHeight() - screenY;
        for (PointWithAction p : pointList) {
            if ((Math.abs(p.getPointX() - screenX ) < p.getCircleRadius()) && (Math.abs(p.getPointY() - screenY) < p.getCircleRadius())) {
                p.setColor(Color.GREEN);
                line = new Line(p,p);
                line.setDefaultColor();
                lineList.addToList(line);
            }
        }
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        screenY = Gdx.graphics.getHeight() - screenY;
        for (PointWithAction p : pointList) {
            if ((Math.abs(p.getPointX() - screenX ) < p.getCircleRadius()) && (Math.abs(p.getPointY() - screenY) < p.getCircleRadius()) && line.getColor().equals(Color.WHITE)) {
                line.setPoint2(p);
                line.setCanRemove(false);
                System.out.println("Line done!!!!");
                return true;
            }
        }
        if (line.isCanRemove()){
            lineList.remove(line);
        }
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        screenY = Gdx.graphics.getHeight() - screenY;
        if (line.isCanRemove()) {
            line.setPoint2(new Point(screenX, screenY));
            for (PointWithAction p : pointList) {
                if ((Math.abs(p.getPointX() - screenX) < p.getCircleRadius()) && (Math.abs(p.getPointY() - screenY) < p.getCircleRadius())) {
                    p.setColor(Color.GREEN);
                } else
                    p.setDefaultColor();
            }

            if (lineList.getList().size() > 1) {
                for (Line l : lineList.getList().subList(0, lineList.getList().size() - 1)) {
                    line.setDefaultColor();
                    if (lineList.isCollision(l.getPoint1(), l.getPoint2(), line.getPoint1(), line.getPoint2())) {
                        System.out.println("x = " + screenX + " y = " + screenY);
                        System.out.println("Collisioin with " + l.toString() + " id = " + lineList.getList().indexOf(l));
                        line.setColor(Color.RED);
                    }
                }
            }
        }

        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        screenY = Gdx.graphics.getHeight() - screenY;
        for (PointWithAction p : pointList) {
            if ((Math.abs(p.getPointX() - screenX ) < p.getCircleRadius()) && (Math.abs(p.getPointY() - screenY) < p.getCircleRadius())) {
                p.setColor(Color.RED);
                return true;
            }
            p.setDefaultColor();
        }
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
