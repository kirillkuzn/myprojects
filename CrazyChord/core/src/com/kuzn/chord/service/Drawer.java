package com.kuzn.chord.service;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.kuzn.chord.model.Line;
import com.kuzn.chord.model.Point;
import com.kuzn.chord.model.PointWithAction;

public class Drawer {

    private ShapeRenderer renderer;

    public Drawer(ShapeRenderer renderer){
        this.renderer = renderer;

    }

    public void drawCircle(PointWithAction p, Color color){
        renderer.setColor(color);
        renderer.circle(p.getPointX(), p.getPointY(), p.getCircleRadius());
    }

    public void drawLine(Line line, Color color){
        renderer.setColor(color);
        renderer.line(line.getPoint1().getPointX(), line.getPoint1().getPointY(), line.getPoint2().getPointX(), line.getPoint2().getPointY());
    }

}
