package com.kuzn.chord.service;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.kuzn.chord.model.Circle;
import com.kuzn.chord.model.Point;
import com.kuzn.chord.model.PointWithAction;

import java.util.ArrayList;
import java.util.List;

public class PointPlacer {

    private Circle circle;
    private int N;
    private List<PointWithAction> pointList;
    ShapeRenderer shapeRenderer;
    List<Integer> list;

    public PointPlacer(Circle circle, int N, ShapeRenderer shapeRenderer){
        this.circle = circle;
        this.N = N;
        this.shapeRenderer = shapeRenderer;
        pointList = new ArrayList<PointWithAction>();
        list = new ArrayList<Integer>();
        generatePoint();
    }

    //Generate N points on circle
    public void generatePoint(){
        int x = 0;
        int y = 0;
        PointWithAction p = new PointWithAction();

        while(pointList.size() != N) {
            x = generateX();
            if (x != 0) {

                y = (int) Math.sqrt(Math.abs((double) ((circle.getRadius() * circle.getRadius()) - (x * x))));
                boolean r = randomPosition();

                if (r) {
                    p = new PointWithAction(x + circle.getCenter().getPointX(), y + circle.getCenter().getPointY());
                    if (!pointList.contains(p))
                        pointList.add(p);
                }
                if (!r) {
                    p = new PointWithAction(x + circle.getCenter().getPointX(), -y + circle.getCenter().getPointY());
                    if (!pointList.contains(p))
                        pointList.add(p);
                }
            }
        }

    }

    //Use for set y
    public boolean randomPosition(){
        return Math.random() < 0.5;
    }

    //Generate x with scale
    public int generateX(){
        int x = (int) (( Math.random() * (circle.getRadius() + circle.getRadius())) - circle.getRadius());
        int check = 0;
        int scale = 20;

        if (list.size() == 0){
            list.add(x);
            return x;
        }
        else {
            for (Integer i : list) {
                if (Math.abs(i - x) > scale)
                    check++;
            }
            if (check == list.size()) {
                list.add(x);
                return x;
            }
        }
        return 0;
    }

    public List<PointWithAction> getPointList() {
        return pointList;
    }
}
