package ru.omsk.music;
import javax.sound.midi.MidiChannel;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.Synthesizer;
import javax.sound.sampled.*;
import java.io.File;
import java.io.IOException;


public class Sound{
    public static void main(String[] args) {
        /*File soundFil = new File("Tst.wav");
        try {
            AudioInputStream ais = AudioSystem.getAudioInputStream(soundFil);
            System.out.println(ais.available());

            System.out.println(ais.getFrameLength());
            Clip clip = AudioSystem.getClip();

            clip.open(ais);
            System.out.println(ais.available());
            FloatControl vc = (FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN);

            vc.setValue(0);

            clip.setFramePosition(-10);

            clip.start();

            Thread.sleep(clip.getMicrosecondLength()/1000);

            clip.stop();

            clip.close();

        } catch (UnsupportedAudioFileException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (LineUnavailableException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/

        try {
            Synthesizer synth = MidiSystem.getSynthesizer();
            synth.open();
            MidiChannel[] channels = synth.getChannels();
            channels[0].programChange(0);
            channels[0].noteOn(66, 80);
            Thread.sleep(10000); // in milliseconds
            channels[0].noteOff(65);
            synth.close();
        }  catch (Exception e) {
            e.printStackTrace();
        }
    }
}
