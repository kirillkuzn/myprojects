package ru.omsk.music;

import com.sun.swing.internal.plaf.synth.resources.synth;

import javax.sound.midi.MidiChannel;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.Synthesizer;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AppTest extends JFrame {
    private JButton playButton = new JButton();
    private JTextField input = new JTextField("", 5);

    public AppTest(){
        super();
        this.setBounds(100,100,250,100);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        Container container = this.getContentPane();
        container.setLayout(new GridLayout(3,2,2,2));
        playButton.addActionListener(new ButtonEventListener());
        playButton.setFocusPainted(false);
        container.add(playButton);

    }

    class ButtonEventListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            try {
                Synthesizer synth = MidiSystem.getSynthesizer();
                synth.open();
                MidiChannel[] channels = synth.getChannels();



                    channels[0].programChange(0);
                    channels[0].noteOn(66, 80);
                Thread.sleep(1000);
                channels[0].noteOff(65);
                synth.close();
            }  catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        AppTest app = new AppTest();
        app.setVisible(true);
    }
}
