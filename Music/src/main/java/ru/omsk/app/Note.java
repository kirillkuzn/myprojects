package ru.omsk.app;

import javax.sound.midi.MidiChannel;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Synthesizer;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

import static java.awt.event.KeyEvent.*;

public class Note extends JLabel{

    private int height = 100;
    private int width = 40;
    private int numbers;
    private int startNumber;
    private boolean isPressed = false;
    private Synthesizer synthesizer;
    private MidiChannel[] channels;
    private int octave;
    private int volume = 0;
    private char aChar;
    private int count = 0;
    private int instrumentNumber = 0;

    public Note(final int number) throws MidiUnavailableException {
        octave = 4;
        startNumber = number + 1;
        this.numbers = considerNote(startNumber);
        setSize(width, height);
        addMouseListener(new MouseListener());
        this.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(aChar),"pressed");
        this.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(changeKeyEvent(aChar),0,true),"released");
        this.getActionMap().put("pressed", new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                try {
                    Note note = (Note) e.getSource();
                    note.isPressed = true;
                    if (count == 0) {
                        count++;
                        note.repaint();
                        play();
                    }
                } catch (MidiUnavailableException e1) {
                    e1.printStackTrace();
                }
            }
        });
        this.getActionMap().put("released", new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                    Note note = (Note) e.getSource();
                    note.isPressed = false;
                    note.repaint();
                    stop();
                    count--;
            }
        });
    }
    public int changeKeyEvent(char aChar){
        if (aChar == 'z')
            return VK_Z;
        if (aChar == 'x')
            return VK_X;
        if (aChar == 'c')
            return VK_C;
        if (aChar == 'v')
            return VK_V;
        if (aChar == 'b')
            return VK_B;
        if (aChar == 'n')
            return VK_N;
        if (aChar == 'm')
            return VK_M;
        return 0;
    }

    public void setInstrumentNumber(int instrumentNumber) {
        this.instrumentNumber = instrumentNumber;
    }

    public int considerNote(int number){
        if (number == 1) {
            number = 12 * octave;
            aChar = 'z';
        }
        if (number == 2){
            number = 2 + 12 * octave;
            aChar = 'x';
        }
        if (number == 3) {
            number = 4 + 12 * octave;
            aChar = 'c';
        }
        if (number == 4) {
            number = 5 + 12 * octave;
            aChar = 'v';
        }
        if (number == 5) {
            number = 7 + 12 * octave;
            aChar = 'b';
        }
        if (number == 6) {
            number = 9 + 12 * octave;
            aChar = 'n';
        }
        if (number == 7) {
            number = 11 + 12 * octave;
            aChar = 'm';
        }
        return number;
    }


    public void play() throws MidiUnavailableException {
        synthesizer = MidiSystem.getSynthesizer();
        synthesizer.open();
        channels = synthesizer.getChannels();
        channels[0].programChange(instrumentNumber);
        channels[0].noteOn(numbers, volume);
        System.out.println("Note number " + numbers + " was pressed");
    }

    public void stop(){
        channels[0].noteOff(numbers);
        synthesizer.close();
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);

        g.setColor(Color.white);
        if(isPressed) {
            g.setColor(Color.gray);
        }
        g.fillRect(0,0, getWidth(), getHeight());
        g.setColor(Color.black);
        g.drawRect(0,0, getWidth(), getHeight());
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }

    public void setOctave(int n) {
        this.octave = n;
        numbers = considerNote(startNumber);
    }

    public int getVolume() {
        return volume;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }

    public int getOctave() { return octave; }


    private class MouseListener extends MouseAdapter{

        @Override
        public void mousePressed(MouseEvent e) {
            Note note = (Note) e.getSource();
            note.isPressed = true;
            note.repaint();
            try {
                play();
            } catch (MidiUnavailableException e1) {
                e1.printStackTrace();
            }
        }

        @Override
        public void mouseReleased(MouseEvent e) {
            Note note = (Note) e.getSource();
            note.isPressed = false;
            note.repaint();
            stop();
        }
    }

}
