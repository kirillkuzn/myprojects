package ru.omsk.app;

import javafx.scene.control.Slider;

import javax.sound.midi.MidiUnavailableException;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;

public class App extends JFrame{

    private final int width = 400;
    private final int height = 600;
    private static ArrayList<Note> noteList = new ArrayList<Note>();
    private static ArrayList<DiesNote> diesNoteList = new ArrayList<DiesNote>();

    public App(){
        setSize(new Dimension(height,width));
        Dimension sSize = Toolkit.getDefaultToolkit ().getScreenSize (),
                fSize = getSize ();
        if (fSize.height > sSize.height) {fSize.height = sSize.height;}
        if (fSize.width > sSize.width) {fSize.width = sSize.width;}
        setLocation ((sSize.width - fSize.width)/2,
                (sSize.height - fSize.height)/2);
        setDefaultCloseOperation (EXIT_ON_CLOSE);
        this.setLayout(null);
        setVisible (true);
    }

    public static void printNotes(App app){
        for (int i = 0; i < 7; i++){
            Note n = null;
            try {
                n = new Note(i);
            } catch (MidiUnavailableException e) {
                e.printStackTrace();
            }
            n.setLocation(n.getWidth() * i, 200);
            app.add(n);
            noteList.add(n);
        }
    }

    public static void printDiesNotes(App app){
        for (int i = 0; i < 6; i++){
            if (i != 2) {
                DiesNote dn = new DiesNote(i);
                dn.setLocation(30 + (40 * i), 200);
                app.add(dn);
                diesNoteList.add(dn);
            }
        }
    }

    public static void printVolumeSlider(App app){
        final JSlider slider = new JSlider(SwingConstants.VERTICAL,0, 127, 0);
        slider.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                for (int i = 0; i < noteList.size(); i++){
                    noteList.get(i).setVolume(slider.getValue());
                }
                for (int i = 0; i < diesNoteList.size(); i++){
                    diesNoteList.get(i).setVolume(slider.getValue());
                }
            }
        });
        slider.setBounds(500,0,80,200);

        app.add(slider);
    }

    public static void printChoise(App app){
        final Choice choice = new Choice();
        choice.add("4");
        choice.add("5");
        choice.add("6");
        choice.setLocation(13,26);

        choice.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                System.out.println(Integer.parseInt(choice.getSelectedItem()));
                for (int i = 0; i < noteList.size(); i++){
                    noteList.get(i).setOctave(Integer.parseInt(choice.getSelectedItem()));
                }
                for (int i = 0; i < diesNoteList.size(); i++){
                    diesNoteList.get(i).setOctave(Integer.parseInt(choice.getSelectedItem()));
                }
            }
        });
        app.add(choice);
    }

    public static void printInstrumentChoice(App app){
        final Choice choice = new Choice();
        choice.add("Piano");
        choice.add("Guitar");
        choice.add("String");
        choice.add("Choir");
        choice.add("Flute");
        choice.add("Trumpet");
        choice.setLocation(13,60);

        choice.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                int num = 0;
                if (choice.getSelectedItem().equals("Piano"))
                    num = 0;
                if (choice.getSelectedItem().equals("Guitar"))
                    num = 26;
                if (choice.getSelectedItem().equals("String"))
                    num = 41;
                if (choice.getSelectedItem().equals("Choir"))
                    num = 53;
                if (choice.getSelectedItem().equals("Flute"))
                    num = 74;
                if (choice.getSelectedItem().equals("Trumpet"))
                    num = 57;
                for (int i = 0; i < noteList.size(); i++){
                    noteList.get(i).setInstrumentNumber(num);
                }
                for (int i = 0; i < diesNoteList.size(); i++){
                    diesNoteList.get(i).setInstrumentNumber(num);
                }
            }
        });
        app.add(choice);
    }

    public static void main(String[] args) {
       App app = new App();
        printDiesNotes(app);
        printNotes(app);
        printChoise(app);
        printVolumeSlider(app);
        printInstrumentChoice(app);
    }
}
