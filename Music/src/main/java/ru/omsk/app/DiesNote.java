package ru.omsk.app;

import javax.sound.midi.MidiChannel;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Synthesizer;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import static java.awt.event.KeyEvent.*;

public class DiesNote extends JLabel{

    private int height = 70;
    private int width = 20;
    private int number;
    private int startNumber;
    private boolean isPressed = false;
    private Synthesizer synthesizer;
    private MidiChannel[] channels;
    private int octave;
    private int volume = 0;
    private int count = 0;
    private char aChar;
    private int instrumentNumber = 0;

    public DiesNote(final int number){
        startNumber = number + 1;
        octave = 4;
        this.number = considerNote(startNumber);
        setSize(width, height);
        setBackground(Color.red);
        addMouseListener(new DiesNote.Listener());
        this.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(aChar),"pressed");
        this.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(changeKeyEvent(aChar),0,true),"released");
        this.getActionMap().put("pressed", new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                try {

                    DiesNote note = (DiesNote) e.getSource();
                    note.isPressed = true;
                    if (count == 0) {
                        count++;
                        note.repaint();
                        play();
                    }
                } catch (MidiUnavailableException e1) {
                    e1.printStackTrace();
                }
            }
        });
        this.getActionMap().put("released", new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                DiesNote note = (DiesNote) e.getSource();
                note.isPressed = false;
                note.repaint();
                stop();
                count--;
            }
        });
    }

    public int considerNote(int number){
        if (number == 1) {
            number = 1 + 12 * octave;
            aChar = 's';
        }
        if (number == 2) {
            number = 3 + 12 * octave;
            aChar = 'd';
        }
        if (number == 4) {
            number = 6 + 12 * octave;
            aChar = 'g';
        }
        if (number == 5) {
            number = 8 + 12 * octave;
            aChar = 'h';
        }
        if (number == 6) {
            number = 10 + 12 * octave;
            aChar = 'j';
        }
        return number;
    }

    public int changeKeyEvent(char aChar){
        if (aChar == 's')
            return VK_S;
        if (aChar == 'd')
            return VK_D;
        if (aChar == 'g')
            return VK_G;
        if (aChar == 'h')
            return VK_H;
        if (aChar == 'j')
            return VK_J;
        return 0;
    }

    public void play() throws MidiUnavailableException {
        synthesizer = MidiSystem.getSynthesizer();
        synthesizer.open();
        channels = synthesizer.getChannels();
        channels[0].programChange(instrumentNumber);
        channels[0].noteOn(number, volume);
        System.out.println("Note number " + number + " was pressed");
    }

    public void stop(){
        channels[0].noteOff(number);
        synthesizer.close();
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);

        g.setColor(Color.BLACK);
        if(isPressed) {
            g.setColor(Color.gray);
        }
        g.fillRect(0,0, getWidth(), getHeight());
        g.setColor(Color.black);
        g.drawRect(0,0, getWidth(), getHeight());
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }

    public void setOctave(int n) {
        this.octave = n;
        number = considerNote(startNumber);
    }

    public void setInstrumentNumber(int instrumentNumber) {
        this.instrumentNumber = instrumentNumber;
    }

    public int getVolume() {
        return volume;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }

    class Listener extends MouseAdapter {

        @Override
        public void mousePressed(MouseEvent e) {
            DiesNote note = (DiesNote) e.getSource();
            note.isPressed = true;
            note.repaint();
            try {
                play();
            } catch (MidiUnavailableException e1) {
                e1.printStackTrace();
            }
        }

        @Override
        public void mouseReleased(MouseEvent e) {
            DiesNote note = (DiesNote) e.getSource();
            note.isPressed = false;
            note.repaint();
            stop();
        }
    }
}
