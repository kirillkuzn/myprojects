-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `mydb` ;

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`Categories`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Categories` (
  `idCategory` INT NOT NULL,
  `categoryName` VARCHAR(45) NULL,
  PRIMARY KEY (`idCategory`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Сonsumers`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Сonsumers` (
  `id` INT NOT NULL,
  `name` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Declarers`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Declarers` (
  `id` INT NOT NULL,
  `name` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Lots`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Lots` (
  `id` INT NOT NULL,
  `name` VARCHAR(45) NULL,
  `category` INT NOT NULL DEFAULT 1,
  `lotDate` DATE NULL,
  `consumerId` INT NULL,
  `declarerId` INT NOT NULL,
  PRIMARY KEY (`id`, `declarerId`),
  INDEX `fk_Lots_table11_idx` (`category` ASC),
  INDEX `fk_Lots_Сonsumers1_idx` (`consumerId` ASC),
  INDEX `fk_Lots_Declarers1_idx` (`declarerId` ASC),
  CONSTRAINT `fk_Lots_table11`
    FOREIGN KEY (`category`)
    REFERENCES `mydb`.`Categories` (`idCategory`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Lots_Сonsumers1`
    FOREIGN KEY (`consumerId`)
    REFERENCES `mydb`.`Сonsumers` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Lots_Declarers1`
    FOREIGN KEY (`declarerId`)
    REFERENCES `mydb`.`Declarers` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `mydb`.`Categories`
-- -----------------------------------------------------
START TRANSACTION;
USE `mydb`;
INSERT INTO `mydb`.`Categories` (`idCategory`, `categoryName`) VALUES (1, 'Скульптура');
INSERT INTO `mydb`.`Categories` (`idCategory`, `categoryName`) VALUES (2, 'Картина');
INSERT INTO `mydb`.`Categories` (`idCategory`, `categoryName`) VALUES (3, 'Артифакт');
INSERT INTO `mydb`.`Categories` (`idCategory`, `categoryName`) VALUES (4, 'Ящик');

COMMIT;


-- -----------------------------------------------------
-- Data for table `mydb`.`Сonsumers`
-- -----------------------------------------------------
START TRANSACTION;
USE `mydb`;
INSERT INTO `mydb`.`Сonsumers` (`id`, `name`) VALUES (1, 'Петр');
INSERT INTO `mydb`.`Сonsumers` (`id`, `name`) VALUES (2, 'Эвелина');
INSERT INTO `mydb`.`Сonsumers` (`id`, `name`) VALUES (3, 'Галина');
INSERT INTO `mydb`.`Сonsumers` (`id`, `name`) VALUES (4, 'Наталия');
INSERT INTO `mydb`.`Сonsumers` (`id`, `name`) VALUES (5, 'Антон');
INSERT INTO `mydb`.`Сonsumers` (`id`, `name`) VALUES (6, 'Александр');
INSERT INTO `mydb`.`Сonsumers` (`id`, `name`) VALUES (7, 'Джон');
INSERT INTO `mydb`.`Сonsumers` (`id`, `name`) VALUES (8, 'Гюнтр');

COMMIT;


-- -----------------------------------------------------
-- Data for table `mydb`.`Declarers`
-- -----------------------------------------------------
START TRANSACTION;
USE `mydb`;
INSERT INTO `mydb`.`Declarers` (`id`, `name`) VALUES (1, 'Иван');
INSERT INTO `mydb`.`Declarers` (`id`, `name`) VALUES (2, 'Степан');
INSERT INTO `mydb`.`Declarers` (`id`, `name`) VALUES (3, 'Кирилл');
INSERT INTO `mydb`.`Declarers` (`id`, `name`) VALUES (4, 'Юлия');
INSERT INTO `mydb`.`Declarers` (`id`, `name`) VALUES (5, 'Екатерина');
INSERT INTO `mydb`.`Declarers` (`id`, `name`) VALUES (6, 'Андрей');

COMMIT;


-- -----------------------------------------------------
-- Data for table `mydb`.`Lots`
-- -----------------------------------------------------
START TRANSACTION;
USE `mydb`;
INSERT INTO `mydb`.`Lots` (`id`, `name`, `category`, `lotDate`, `consumerId`, `declarerId`) VALUES (1, 'Скульптура Зевса', 1, '2006.06.14', 3, 1);
INSERT INTO `mydb`.`Lots` (`id`, `name`, `category`, `lotDate`, `consumerId`, `declarerId`) VALUES (2, 'Картина Репина', 2, '2006.06.19', 4, 3);
INSERT INTO `mydb`.`Lots` (`id`, `name`, `category`, `lotDate`, `consumerId`, `declarerId`) VALUES (3, 'Картина Сальвадора Дали', 2, '2006.07.03', 5, 4);
INSERT INTO `mydb`.`Lots` (`id`, `name`, `category`, `lotDate`, `consumerId`, `declarerId`) VALUES (4, 'Меч Александра Первого', 3, '2006.07.20', 8, 6);
INSERT INTO `mydb`.`Lots` (`id`, `name`, `category`, `lotDate`, `consumerId`, `declarerId`) VALUES (5, 'Скульптура Геракла', 1, '2006.09.10', 1, 5);
INSERT INTO `mydb`.`Lots` (`id`, `name`, `category`, `lotDate`, `consumerId`, `declarerId`) VALUES (6, 'Трубка Сталина', 3, '2006.11.06', 3, 2);
INSERT INTO `mydb`.`Lots` (`id`, `name`, `category`, `lotDate`, `consumerId`, `declarerId`) VALUES (7, 'Картина Пабло Пикассо', 2, '2007.02.19', 7, 2);
INSERT INTO `mydb`.`Lots` (`id`, `name`, `category`, `lotDate`, `consumerId`, `declarerId`) VALUES (8, 'Картина Винсенат Ван Гога', 2, '2007.03.11', 5, 3);
INSERT INTO `mydb`.`Lots` (`id`, `name`, `category`, `lotDate`, `consumerId`, `declarerId`) VALUES (10, 'Дневник Ленина', 3, '2008.03.14', 2, 5);
INSERT INTO `mydb`.`Lots` (`id`, `name`, `category`, `lotDate`, `consumerId`, `declarerId`) VALUES (11, 'Черный Ящик', 4, '2008.05.23', NULL, 6);

COMMIT;