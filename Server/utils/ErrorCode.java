package ru.omsu.imit.s2.mainTask.utils;

public enum  ErrorCode {

    SUCCESS("", ""),
    LOT_NOT_FOUND("lot", "Lot not found %s"),
    CATEGORY_NOT_FOUND("category", "Category not found %s"),
    DECLARER_NOT_FOUND("declarer", "Declarer not found %s"),
    NULL_REQUEST("json", "Null request"),
    JSON_PARSE_EXCEPTION("json", "Json parse exception :  %s"),
    WRONG_URL("url", "Wrong URL"),
    METHOD_NOT_ALLOWED("url", "Method not allowed"),
    UNKNOWN_ERROR("error", "Unknown error"),
            ;

    private String field;
    private String message;

    private ErrorCode(String field, String message) {
        this.field = field;
        this.message = message;
    }

    public String getField() {
        return field;
    }

    public String getMessage() {
        return message;
    }

}
