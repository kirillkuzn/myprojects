package ru.omsu.imit.s2.mainTask.utils;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import org.apache.commons.lang.StringUtils;
import ru.omsu.imit.s2.mainTask.exception.LotsException;
import ru.omsu.imit.s2.mainTask.rest.response.FailureResponse;

import javax.ws.rs.core.Response;

public class LotUtils {

    private static final Gson GSON = new Gson();

    public static <T> T getClassInstanceFromJson(Gson gson, String json, Class<T> clazz) throws LotsException {
        if (StringUtils.isEmpty(json)) {
            throw new LotsException(ErrorCode.NULL_REQUEST);
        }
        try {
            return gson.fromJson(json, clazz);
        } catch (JsonSyntaxException ex) {
            throw new LotsException(ErrorCode.JSON_PARSE_EXCEPTION, json);
        }
    }

    public static Response failureResponse(Response.Status status, LotsException ex) {
        return Response.status(status).entity(GSON.toJson(new FailureResponse(ex.getErrorCode(), ex.getMessage()))).build();
    }

    public static Response failureResponse(LotsException ex) {
        return failureResponse(Response.Status.BAD_REQUEST, ex);
    }
}
