package ru.omsu.imit.s2.mainTask.model;

public class Category {

    private int id;
    private String name;

    public Category(int idCategory, String categoryName) {
        this.id = idCategory;
        this.name = categoryName;
    }

    public Category(){};

    public int getId() {
        return id;
    }

    public void setId(int idCategory) {
        this.id = idCategory;
    }

    public String getName() {
        return name;
    }

    public void setName(String categoryName) {
        this.name = categoryName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Category)) return false;

        Category that = (Category) o;

        if (id != that.id) return false;
        return name != null ? name.equals(that.name) : that.name == null;

    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Category{" +
                "idCategory=" + id +
                ", categoryName='" + name + '\'' +
                '}';
    }
}
