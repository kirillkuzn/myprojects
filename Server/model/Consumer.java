package ru.omsu.imit.s2.mainTask.model;

public class Consumer {

    private int id;
    private String name;

    public Consumer(int idConsumers, String consumersName) {
        this.id = idConsumers;
        this.name = consumersName;
    }

    public Consumer(){ }

    public int getId() {
        return id;
    }

    public void setId(int idConsumers) {
        this.id = idConsumers;
    }

    public String getName() {
        return name;
    }

    public void setName(String consumersName) {
        this.name = consumersName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Consumer)) return false;

        Consumer consumer = (Consumer) o;

        if (id != consumer.id) return false;
        return name != null ? name.equals(consumer.name) : consumer.name == null;

    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Consumer{" +
                "idConsumers=" + id +
                ", consumersName='" + name + '\'' +
                '}';
    }
}
