package ru.omsu.imit.s2.mainTask.model;

public class Declarer {

    private int id;
    private String name;

    public Declarer(int declarerId, String declarerName) {
        this.id = declarerId;
        this.name = declarerName;
    }

    public Declarer(){}

    public int getId() {
        return id;
    }

    public void setId(int declarerId) {
        this.id = declarerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String declarerName) {
        this.name = declarerName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Declarer)) return false;

        Declarer declarer = (Declarer) o;

        if (id != declarer.id) return false;
        return name != null ? name.equals(declarer.name) : declarer.name == null;

    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Declarer{" +
                "id=" + id +
                ", declarerName='" + name + '\'' +
                '}';
    }
}
