package ru.omsu.imit.s2.mainTask.model;

import java.util.Date;

public class Lot {

    private int id;
    private String name;
    private Category category;
    private Date lotDate;
    private Consumer consumer;
    private Declarer declarer;

    public Lot(int idLots, String nameLot, Category category, Date lotDate, Consumer consumer, Declarer declarer) {
        this.id = idLots;
        this.name = nameLot;
        this.category = category;
        this.lotDate = lotDate;
        this.consumer = consumer;
        this.declarer = declarer;
    }

    public Lot(){}

    public int getId() {
        return id;
    }

    public void setId(int idLots) {
        this.id = idLots;
    }

    public String getName() {
        return name;
    }

    public void setName(String nameLot) {
        this.name = nameLot;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Date getLotDate() {
        return lotDate;
    }

    public void setLotDate(Date lotDate) {
        this.lotDate = lotDate;
    }

    public Consumer getConsumer() {
        return consumer;
    }

    public void setConsumer(Consumer consumer) {
        this.consumer = consumer;
    }

    public Declarer getDeclarer() {
        return declarer;
    }

    public void setDeclarer(Declarer declarer) {
        this.declarer = declarer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Lot)) return false;

        Lot lot = (Lot) o;

        if (id != lot.id) return false;
        if (name != null ? !name.equals(lot.name) : lot.name != null) return false;
        if (category != null ? !category.equals(lot.category) : lot.category != null) return false;
        if (lotDate != null ? !lotDate.equals(lot.lotDate) : lot.lotDate != null) return false;
        if (consumer != null ? !consumer.equals(lot.consumer) : lot.consumer != null) return false;
        return declarer != null ? declarer.equals(lot.declarer) : lot.declarer == null;

    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (category != null ? category.hashCode() : 0);
        result = 31 * result + (lotDate != null ? lotDate.hashCode() : 0);
        result = 31 * result + (consumer != null ? consumer.hashCode() : 0);
        result = 31 * result + (declarer != null ? declarer.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Lot{" +
                "idLots=" + id +
                ", nameLot='" + name + '\'' +
                ", category=" + category.toString() +
                ", lotDate=" + lotDate +
                ", consumer=" + consumer.toString() +
                ", declarer=" + declarer.toString() +
                '}';
    }
}
