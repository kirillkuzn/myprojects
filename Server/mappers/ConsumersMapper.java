package ru.omsu.imit.s2.mainTask.mappers;

import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import ru.omsu.imit.s2.mainTask.model.Category;
import ru.omsu.imit.s2.mainTask.model.Consumer;

public interface ConsumersMapper {

    @Select("select * from Сonsumers where id = #{id}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name")
    })
    Consumer getById(int id);

}
