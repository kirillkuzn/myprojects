package ru.omsu.imit.s2.mainTask.mappers;

import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.FetchType;
import ru.omsu.imit.s2.mainTask.model.Category;
import ru.omsu.imit.s2.mainTask.model.Consumer;
import ru.omsu.imit.s2.mainTask.model.Declarer;
import ru.omsu.imit.s2.mainTask.model.Lot;

import java.util.List;

public interface LotsMapper {

    @Select("select * from Lots")
    @Results({
        @Result(property="id", column="id"),
        @Result(property="name", column="nameLot"),
        @Result(property = "category", column = "category", javaType = Category.class, one = @One(select = "ru.omsu.imit.s2.mainTask.mappers.CategoriesMapper.getById",fetchType = FetchType.LAZY)),
        @Result(property = "lotDate", column = "lotDate"),
        @Result(property = "consumer", column = "consumerId", javaType = Consumer.class, one = @One(select = "ru.omsu.imit.s2.mainTask.mappers.ConsumersMapper.getById", fetchType = FetchType.EAGER)),
        @Result(property = "declarer", column = "declarerId", javaType = Declarer.class, one = @One(select = "ru.omsu.imit.s2.mainTask.mappers.DeclarersMapper.getById", fetchType = FetchType.EAGER))
    })
    List<Lot> getAllLots();

    @Select("select * from Lots where id = #{id}")
    @Results({
            @Result(property="id", column="id"),
            @Result(property="name", column="name"),
            @Result(property = "category", column = "category", javaType = Category.class, one = @One(select = "ru.omsu.imit.s2.mainTask.mappers.CategoriesMapper.getById",fetchType = FetchType.LAZY))
    })
    Lot getLotById(@Param("id") int id);

    @Select("select * from Lots where category = #{id}")
    @Results({
            @Result(property="idLots", column="idLots"),
            @Result(property="nameLot", column="nameLot"),
            @Result(property = "category", column = "category", javaType = Category.class, one = @One(select = "ru.omsu.imit.s2.mainTask.mappers.CategoriesMapper.getById",fetchType = FetchType.LAZY))
    })
    List<Lot> getByCategories(@Param("id") int id);

    @Insert("insert into Lots (idLots, nameLot, category) values" + " ( #{idLots}, #{nameLot}, #{category.idCategory} )")
    @Options(useGeneratedKeys = true)
    Integer insert(Lot lot);

    @Delete("delete from Lots where idLots = #{id}")
    int delete(@Param("id") int id);
}
