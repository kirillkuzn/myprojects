package ru.omsu.imit.s2.mainTask.mappers;

import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.FetchType;
import ru.omsu.imit.s2.mainTask.model.Category;
import ru.omsu.imit.s2.mainTask.model.Declarer;
import ru.omsu.imit.s2.mainTask.model.Lot;

import java.util.List;

public interface DeclarersMapper {

    @Select("select * from Declarers")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name")
    })
    public List<Declarer> getAllDeclarers();

    @Select("select * from Declarers where id = #{id}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name")
    })
    Declarer getById(@Param("id") int id);


}
