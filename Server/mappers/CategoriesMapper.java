package ru.omsu.imit.s2.mainTask.mappers;

import org.apache.ibatis.annotations.*;
import ru.omsu.imit.s2.mainTask.model.Category;
import ru.omsu.imit.s2.mainTask.model.Lot;

import java.util.List;

public interface CategoriesMapper {

    @Select("select * from Categories")
    @Results({
        @Result(property = "id", column = "id"),
        @Result(property = "name", column = "name")
    })
    public List<Category> getAllCategories();

    @Select("select * from Categories where id = #{id}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name")
    })
    public Category getById(int id);

    @Insert("insert into Categories (id, name) values" + " ( #{id}, #{name} )")
    @Options(useGeneratedKeys = true)
    Integer insert(Category category);

    @Delete("delete from Categories where idCategory = #{id}")
    int delete(@Param("id") int id);
}
