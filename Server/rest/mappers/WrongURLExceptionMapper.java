package ru.omsu.imit.s2.mainTask.rest.mappers;

import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import ru.omsu.imit.s2.mainTask.exception.LotsException;
import ru.omsu.imit.s2.mainTask.utils.LotUtils;
import ru.omsu.imit.s2.mainTask.utils.ErrorCode;



@Provider
public class WrongURLExceptionMapper implements	ExceptionMapper<NotFoundException> {

    @Override
	public Response toResponse(NotFoundException exception) {
		return LotUtils.failureResponse(Status.NOT_FOUND, new LotsException(ErrorCode.WRONG_URL));
	}
}