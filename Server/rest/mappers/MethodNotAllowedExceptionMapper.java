package ru.omsu.imit.s2.mainTask.rest.mappers;

import javax.ws.rs.NotAllowedException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import ru.omsu.imit.s2.mainTask.exception.LotsException;
import ru.omsu.imit.s2.mainTask.utils.LotUtils;
import ru.omsu.imit.s2.mainTask.utils.ErrorCode;


@Provider
public class MethodNotAllowedExceptionMapper implements	ExceptionMapper<NotAllowedException> {

    @Override
	public Response toResponse(NotAllowedException exception) {
		return LotUtils.failureResponse(Status.METHOD_NOT_ALLOWED, new LotsException(ErrorCode.METHOD_NOT_ALLOWED));
	}
}