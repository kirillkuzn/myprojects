package ru.omsu.imit.s2.mainTask.rest.mappers;

import com.fasterxml.jackson.core.JsonProcessingException;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import ru.omsu.imit.s2.mainTask.exception.LotsException;
import ru.omsu.imit.s2.mainTask.utils.ErrorCode;
import ru.omsu.imit.s2.mainTask.utils.LotUtils;


@Provider
public class JsonProcessingExceptionMapper implements
		ExceptionMapper<JsonProcessingException> {
	@Override
    public Response toResponse(JsonProcessingException exception) {
		return LotUtils.failureResponse(Status.BAD_REQUEST, new LotsException(ErrorCode.JSON_PARSE_EXCEPTION));
    }
}