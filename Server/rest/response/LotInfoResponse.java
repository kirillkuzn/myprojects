package ru.omsu.imit.s2.mainTask.rest.response;

import ru.omsu.imit.s2.mainTask.model.Lot;

public class LotInfoResponse {

    private Lot lot;


    public LotInfoResponse(Lot lot){
        this.lot = lot;
    }

    protected LotInfoResponse() {
    }

    public Lot getLot(){
        return lot;
    }

}
