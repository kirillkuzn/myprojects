package ru.omsu.imit.s2.mainTask.rest.response;

import ru.omsu.imit.s2.mainTask.model.Category;

public class CategoryInfoResponce {

    private Category category;

    public CategoryInfoResponce(Category category){
        this.category = category;
    }

    public CategoryInfoResponce(){}

    public Category getCategory(){
        return category;
    }

}
