package ru.omsu.imit.s2.mainTask.rest.response;

import ru.omsu.imit.s2.mainTask.model.Declarer;

public class DeclarerInfoResponse {

    private Declarer declarer;

    public DeclarerInfoResponse(Declarer declarer) {
        this.declarer = declarer;
    }

    public DeclarerInfoResponse(){}

    public Declarer getDeclarer() {
        return declarer;
    }
}
