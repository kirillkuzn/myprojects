package ru.omsu.imit.s2.mainTask.rest.request;

public class LotRequest {

    private String text;

    public LotRequest(String text) {
        super();
        this.text = text;
    }

    protected LotRequest() {
    }

    public String getText() {
        return text;
    }
}
