package ru.omsu.imit.s2.mainTask.resources;

import ru.omsu.imit.s2.mainTask.service.DeclarersService;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

@Path("/api")
public class DeclarersResourse {

    private DeclarersService declarersService = new DeclarersService();

    @GET
    @Path("/declarer/{id}")
    @Produces("application/json")
    public Response getById(@PathParam(value = "id") int id) {
        return declarersService.getById(id);
    }

    @GET
    @Path("/declarer/")
    @Produces("application/json")
    public Response getAll() {
        return declarersService.getAll();
    }

}
