package ru.omsu.imit.s2.mainTask.resources;


import ru.omsu.imit.s2.mainTask.dao.CategoriesDAO;
import ru.omsu.imit.s2.mainTask.service.CategoriesService;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

@Path("/api")
public class CategoriesResourse {

    private CategoriesService categoriesService = new CategoriesService();

    @POST
    @Path("/category")
    @Consumes("application/json")
    @Produces("application/json")
    public Response addLot(String json) {
        return categoriesService.insertCategory(json);
    }

    @DELETE
    @Path("/category/{id}")
    @Produces("application/json")
    public Response deleteById(@PathParam(value = "id") int id) { return categoriesService.deleteCategory(id); }

    @GET
    @Path("/category/{id}")
    @Produces("application/json")
    public Response getById(@PathParam(value = "id") int id) {
        return categoriesService.getCategoryById(id);
    }

    @GET
    @Path("/category/")
    @Produces("application/json")
    public Response getAll() {
        return categoriesService.getAllCategories();
    }
}
