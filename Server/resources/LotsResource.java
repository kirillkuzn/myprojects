package ru.omsu.imit.s2.mainTask.resources;

import ru.omsu.imit.s2.mainTask.service.LotService;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

@Path("/api")
public class LotsResource {

    private static LotService lotService = new LotService();

    @POST
    @Path("/lot")
    @Consumes("application/json")
    @Produces("application/json")
    public Response addLot(String json) {
        return lotService.insertLot(json);
    }

    @GET
    @Path("/lot/{id}")
    @Produces("application/json")
    public Response getById(@PathParam(value = "id") int id) {
        return lotService.getById(id);
    }

    @GET
    @Path("/lot/")
    @Produces("application/json")
    public Response getAll() {
        return lotService.getAllLot();
    }

    @DELETE
    @Path("/lot/{id}")
    @Produces("application/json")
    public Response deleteById(@PathParam(value = "id") int id) { return lotService.deleteById(id); }
}
