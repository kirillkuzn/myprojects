package ru.omsu.imit.s2.mainTask.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.omsu.imit.s2.mainTask.dao.CategoriesDAO;
import ru.omsu.imit.s2.mainTask.daoimpl.CategoriesDAOImpl;
import ru.omsu.imit.s2.mainTask.exception.LotsException;
import ru.omsu.imit.s2.mainTask.model.Category;
import ru.omsu.imit.s2.mainTask.model.Lot;
import ru.omsu.imit.s2.mainTask.rest.response.CategoryInfoResponce;
import ru.omsu.imit.s2.mainTask.rest.response.LotInfoResponse;
import ru.omsu.imit.s2.mainTask.rest.response.SuccessResponce;
import ru.omsu.imit.s2.mainTask.utils.LotUtils;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

public class CategoriesService {

    private static final Logger LOGGER = LoggerFactory.getLogger(LotService.class);
    private static final Gson GSON = new GsonBuilder().create();
    private CategoriesDAO categoriesDAO = new CategoriesDAOImpl();


    public Response getCategoryById(int id) {
        LOGGER.debug("get By id " + id);
        try {
            Category category = categoriesDAO.getById(id);
            String response = GSON.toJson(new CategoryInfoResponce(category));
            return Response.ok(response, MediaType.APPLICATION_JSON).build();
        } catch (LotsException e) {
            return LotUtils.failureResponse(e);
        }
    }

    public Response getAllCategories(){
        LOGGER.debug("get all categories");
        List<Category> categories = categoriesDAO.getAll();
        List<CategoryInfoResponce> responseList = new ArrayList<>();
        for (Category l: categories){
            responseList.add(new CategoryInfoResponce(l));
        }
        String response = GSON.toJson(responseList);
        return Response.ok(response,MediaType.APPLICATION_JSON_TYPE).build();
    }

    public Response insertCategory(String json){
        LOGGER.debug("insert category");
        try {
            Category request = LotUtils.getClassInstanceFromJson(GSON,json,Category.class);
            categoriesDAO.addCategory(request);
            String response = GSON.toJson(new SuccessResponce());
            return Response.ok(response, MediaType.APPLICATION_JSON).build();
        } catch (LotsException e) {
            return LotUtils.failureResponse(e);
        }
    }

    public Response deleteCategory(int id){
        LOGGER.debug("delete category");
        try{
            categoriesDAO.deleteCategory(id);
            String response = GSON.toJson(new SuccessResponce());
            return Response.ok(response, MediaType.APPLICATION_JSON).build();
        } catch (LotsException e) {
            return LotUtils.failureResponse(e);
        }
    }
}
