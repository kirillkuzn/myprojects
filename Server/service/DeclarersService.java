package ru.omsu.imit.s2.mainTask.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.omsu.imit.s2.mainTask.dao.DeclarersDAO;
import ru.omsu.imit.s2.mainTask.dao.LotDAO;
import ru.omsu.imit.s2.mainTask.daoimpl.DeclarersDAOImpl;
import ru.omsu.imit.s2.mainTask.daoimpl.LotDAOImpl;
import ru.omsu.imit.s2.mainTask.exception.LotsException;
import ru.omsu.imit.s2.mainTask.model.Declarer;
import ru.omsu.imit.s2.mainTask.model.Lot;
import ru.omsu.imit.s2.mainTask.rest.response.DeclarerInfoResponse;
import ru.omsu.imit.s2.mainTask.rest.response.LotInfoResponse;
import ru.omsu.imit.s2.mainTask.utils.LotUtils;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

public class DeclarersService {

    private static final Logger LOGGER = LoggerFactory.getLogger(LotService.class);
    private static final Gson GSON = new GsonBuilder().create();
    private DeclarersDAO declarersDAO = new DeclarersDAOImpl();

    public Response getAll(){
        LOGGER.debug("get all declarers");
        List<Declarer> declarers = declarersDAO.getAll();
        List<DeclarerInfoResponse> list = new ArrayList<>();
        for (Declarer d: declarers){
            list.add(new DeclarerInfoResponse(d));
        }
        String response = GSON.toJson(list);
        return Response.ok(response, MediaType.APPLICATION_JSON_TYPE).build();
    }

    public Response getById(int id) {
        LOGGER.debug("get declarer by id " + id);
        try {
            Declarer declarer = declarersDAO.getById(id);
            String response = GSON.toJson(new DeclarerInfoResponse(declarer));
            return Response.ok(response, MediaType.APPLICATION_JSON).build();
        } catch (LotsException e) {
            return LotUtils.failureResponse(e);
        }
    }

}
