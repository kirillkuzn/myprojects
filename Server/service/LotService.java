package ru.omsu.imit.s2.mainTask.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.omsu.imit.s2.mainTask.dao.LotDAO;
import ru.omsu.imit.s2.mainTask.daoimpl.LotDAOImpl;
import ru.omsu.imit.s2.mainTask.exception.LotsException;
import ru.omsu.imit.s2.mainTask.model.Lot;
import ru.omsu.imit.s2.mainTask.rest.response.LotInfoResponse;
import ru.omsu.imit.s2.mainTask.rest.response.SuccessResponce;
import ru.omsu.imit.s2.mainTask.utils.LotUtils;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

public class LotService {

    private static final Logger LOGGER = LoggerFactory.getLogger(LotService.class);
    private static final Gson GSON = new GsonBuilder().create();
    private LotDAO lotDAO = new LotDAOImpl();

    public Response insertLot(String json){
        LOGGER.debug("Insert lot " + json);
        try{
            Lot request = LotUtils.getClassInstanceFromJson(GSON, json, Lot.class);
            lotDAO.addLot(request);
            String response = GSON.toJson(new SuccessResponce());
            return Response.ok(response, MediaType.APPLICATION_JSON).build();
        } catch (LotsException e) {
            return LotUtils.failureResponse(e);
        }
    }

    public Response deleteById(int id){
        LOGGER.debug("delete lot by " + id + " id");
        try{
            lotDAO.deleteLot(id);
            String response = GSON.toJson(new SuccessResponce());
            return Response.ok(response, MediaType.APPLICATION_JSON).build();
        } catch (LotsException e) {
            return LotUtils.failureResponse(e);
        }
    }

    public Response getById(int id) {
        LOGGER.debug("get By id " + id);
        try {
            Lot lot = lotDAO.getById(id);
            String response = GSON.toJson(new LotInfoResponse(lot));
            return Response.ok(response, MediaType.APPLICATION_JSON).build();
        } catch (LotsException e) {
            return LotUtils.failureResponse(e);
        }
    }

    public Response getAllLot(){
        LOGGER.info("get all lots");
        List<Lot> lots = lotDAO.getAll();
        List<LotInfoResponse> responseList = new ArrayList<>();
        for (Lot l: lots){
            responseList.add(new LotInfoResponse(l));
        }
        String response = GSON.toJson(responseList);
        return Response.ok(response,MediaType.APPLICATION_JSON_TYPE).build();
    }
}
