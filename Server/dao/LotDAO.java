package ru.omsu.imit.s2.mainTask.dao;

import ru.omsu.imit.s2.mainTask.exception.LotsException;
import ru.omsu.imit.s2.mainTask.model.Category;
import ru.omsu.imit.s2.mainTask.model.Lot;

import java.util.List;

public interface LotDAO {

    public List<Lot> getAll();
    public Lot getById(int id) throws LotsException;
    public List<Lot> getByCategories(Category category);
    public Integer addLot(Lot lot) throws LotsException;
    public int deleteLot(int id) throws LotsException;
}
