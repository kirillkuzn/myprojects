package ru.omsu.imit.s2.mainTask.dao;

import ru.omsu.imit.s2.mainTask.exception.LotsException;
import ru.omsu.imit.s2.mainTask.model.Category;

import java.util.List;

public interface CategoriesDAO {

    public List<Category> getAll();
    public Category getById(int id) throws LotsException;
    public int addCategory(Category category) throws LotsException;
    int deleteCategory(int id) throws LotsException;
}
