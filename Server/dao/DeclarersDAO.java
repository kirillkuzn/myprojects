package ru.omsu.imit.s2.mainTask.dao;

import ru.omsu.imit.s2.mainTask.exception.LotsException;
import ru.omsu.imit.s2.mainTask.model.Declarer;

import java.util.List;

public interface DeclarersDAO {

    List<Declarer> getAll();
    Declarer getById(int id) throws LotsException;
}
