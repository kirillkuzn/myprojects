package ru.omsu.imit.s2.mainTask.daoimpl;

import org.apache.ibatis.session.SqlSession;
import ru.omsu.imit.s2.mainTask.dao.DeclarersDAO;
import ru.omsu.imit.s2.mainTask.exception.LotsException;
import ru.omsu.imit.s2.mainTask.model.Declarer;
import ru.omsu.imit.s2.mainTask.utils.ErrorCode;

import java.util.List;

public class DeclarersDAOImpl extends BaseDAOImpl implements DeclarersDAO {
    @Override
    public List<Declarer> getAll() {
        try(SqlSession sqlSession = getSession()){
            return getDeclarersMapper(sqlSession).getAllDeclarers();
        }
    }

    @Override
    public Declarer getById(int id) throws LotsException {
        try(SqlSession sqlSession = getSession()){
            Declarer declarer = getDeclarersMapper(sqlSession).getById(id);
            if (declarer == null)
                throw new LotsException(ErrorCode.DECLARER_NOT_FOUND);
            return declarer;
        }
    }
}
