package ru.omsu.imit.s2.mainTask.daoimpl;

import org.apache.ibatis.session.SqlSession;
import ru.omsu.imit.s2.mainTask.mappers.CategoriesMapper;
import ru.omsu.imit.s2.mainTask.mappers.DeclarersMapper;
import ru.omsu.imit.s2.mainTask.mappers.LotsMapper;
import ru.omsu.imit.s2.mainTask.utils.MyBatisUtils;

public class BaseDAOImpl {

    protected SqlSession getSession(){
        return MyBatisUtils.getSqlSessionFactory().openSession();
    }

    protected LotsMapper getLotsMapper(SqlSession sqlSession){
        return sqlSession.getMapper(LotsMapper.class);
    }

    protected CategoriesMapper getCategoriesMapper(SqlSession sqlSession){
        return sqlSession.getMapper(CategoriesMapper.class);
    }

    protected DeclarersMapper getDeclarersMapper(SqlSession sqlSession){
        return sqlSession.getMapper(DeclarersMapper.class);
    }

}
