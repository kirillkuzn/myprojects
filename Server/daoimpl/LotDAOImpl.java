package ru.omsu.imit.s2.mainTask.daoimpl;

import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.omsu.imit.s2.mainTask.dao.LotDAO;
import ru.omsu.imit.s2.mainTask.exception.LotsException;
import ru.omsu.imit.s2.mainTask.model.Category;
import ru.omsu.imit.s2.mainTask.model.Lot;
import ru.omsu.imit.s2.mainTask.utils.ErrorCode;

import java.util.List;

public class LotDAOImpl extends BaseDAOImpl implements LotDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(LotDAOImpl.class);


    @Override
    public List<Lot> getAll() {
        try(SqlSession sqlSession = getSession()) {
            return getLotsMapper(sqlSession).getAllLots();
        }
    }

    @Override
    public Lot getById(int id) throws LotsException {
        try(SqlSession sqlSession = getSession()){
            Lot lot =  getLotsMapper(sqlSession).getLotById(id);
            if (lot == null)
                throw new LotsException(ErrorCode.LOT_NOT_FOUND);
            return lot;
        }
    }

    @Override
    public List<Lot> getByCategories(Category category) {
        try(SqlSession sqlSession = getSession()) {
            return getLotsMapper(sqlSession).getByCategories(category.getId());
        }
    }

    @Override
    public Integer addLot(Lot lot) throws LotsException {
        try(SqlSession sqlSession = getSession()){
            int addedLot =  getLotsMapper(sqlSession).insert(lot);
            if (addedLot == 0)
                throw new LotsException(ErrorCode.UNKNOWN_ERROR);
            sqlSession.commit();
            return addedLot;
        }
    }

    @Override
    public int deleteLot(int id) throws LotsException {
        try(SqlSession sqlSession = getSession()){
            int addedLot =  getLotsMapper(sqlSession).delete(id);
            if (addedLot == 0)
                throw new LotsException(ErrorCode.LOT_NOT_FOUND);
            sqlSession.commit();
            return addedLot;
        }
    }
}
