package ru.omsu.imit.s2.mainTask.daoimpl;

import org.apache.ibatis.session.SqlSession;
import ru.omsu.imit.s2.mainTask.dao.CategoriesDAO;
import ru.omsu.imit.s2.mainTask.exception.LotsException;
import ru.omsu.imit.s2.mainTask.model.Category;
import ru.omsu.imit.s2.mainTask.utils.ErrorCode;

import java.util.List;

public class CategoriesDAOImpl extends BaseDAOImpl implements CategoriesDAO {
    @Override
    public List<Category> getAll() {
        try(SqlSession sqlSession = getSession()){
            return getCategoriesMapper(sqlSession).getAllCategories();
        }
    }

    @Override
    public Category getById(int id) throws LotsException {
        try(SqlSession sqlSession = getSession()){
            Category category = getCategoriesMapper(sqlSession).getById(id);
            if (category == null)
                throw new LotsException(ErrorCode.CATEGORY_NOT_FOUND);
            return category;
        }
    }

    @Override
    public int addCategory(Category category) throws LotsException {
        try(SqlSession sqlSession = getSession()){
            int r = getCategoriesMapper(sqlSession).insert(category);
            if (r == 0)
                throw new LotsException(ErrorCode.UNKNOWN_ERROR);
            sqlSession.commit();
            return r;
        }
    }

    @Override
    public int deleteCategory(int id) throws LotsException {
        try(SqlSession sqlSession = getSession()) {
            int r = getCategoriesMapper(sqlSession).delete(id);
            if (r == 0)
                throw new LotsException(ErrorCode.CATEGORY_NOT_FOUND);
            sqlSession.commit();
            return r;
        }
    }
}
