package ru.omsu.imit.s2.mainTask.server.config;
import org.glassfish.jersey.server.ResourceConfig;

public class LotResourseConfig extends ResourceConfig {
    public LotResourseConfig() {
        packages("ru.omsu.imit.s2.mainTask.resources",
                "ru.omsu.imit.s2.mainTask.rest.mappers");
    }
}
