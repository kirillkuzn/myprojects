package ru.omsu.imit.s2.mainTask.server;

import org.eclipse.jetty.server.Server;
import org.glassfish.jersey.jetty.JettyHttpContainerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.omsu.imit.s2.mainTask.server.config.LotResourseConfig;
import ru.omsu.imit.s2.mainTask.server.config.Settings;
import ru.omsu.imit.s2.mainTask.utils.MyBatisUtils;

import javax.ws.rs.core.UriBuilder;
import java.net.URI;

public class LotServer {

    private static final Logger LOGGER = LoggerFactory.getLogger(LotServer.class);

    private static Server jettyServer;

    private static void attachShutDownHook() {
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                stopServer();
            }
        });
    }

    public static void createServer() {
        URI baseUri = UriBuilder.fromUri("http://localhost/").port(Settings.getRestHttpPort()).build();
        LotResourseConfig config = new LotResourseConfig();
        jettyServer = JettyHttpContainerFactory.createServer(baseUri, config);
        LOGGER.info("Server started at port " + Settings.getRestHttpPort());
        MyBatisUtils.initSqlSessionFactory();
    }

    public static void stopServer() {
        LOGGER.info("Stopping server");
        try {
            jettyServer.stop();
            jettyServer.destroy();
        } catch (Exception e) {
            LOGGER.error("Error stopping service", e);
            System.exit(1);
        }
        LOGGER.info("Server stopped");
    }


    public static void main(String[] args) {
        attachShutDownHook();
        createServer();
    }

}
